<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Empleado
 *
 * @ORM\Table(name="empleado")
 * @ORM\Entity
 */
class Empleado
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=11, nullable=false)
     * @Assert\NotBlank()
     */
    private $nombre = '';

    /**
     * @var string
     *
     * @ORM\Column(name="apellido_paterno", type="string", length=11, nullable=false)
     * @Assert\NotBlank()
     */
    private $apellidoPaterno = '';

    /**
     * @var string
     *
     * @ORM\Column(name="apellido_materno", type="string", length=11, nullable=false)
     * @Assert\NotBlank()
     */
    private $apellidoMaterno = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_de_nacimiento", type="date", nullable=false)
     * @Assert\NotBlank()
     */
    private $fechaDeNacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="ingresos_anuales", type="decimal", precision=10, scale=2, nullable=false)
     * @Assert\NotBlank()
     */
    private $ingresosAnuales;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Assert\NotBlank()
     */
    private $id;



    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Empleado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     *
     * @return Empleado
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     *
     * @return Empleado
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set fechaDeNacimiento
     *
     * @param \DateTime $fechaDeNacimiento
     *
     * @return Empleado
     */
    public function setFechaDeNacimiento($fechaDeNacimiento)
    {
        $this->fechaDeNacimiento = $fechaDeNacimiento;

        return $this;
    }

    /**
     * Get fechaDeNacimiento
     *
     * @return \DateTime
     */
    public function getFechaDeNacimiento()
    {
        return $this->fechaDeNacimiento;
    }

    /**
     * Set ingresosAnuales
     *
     * @param string $ingresosAnuales
     *
     * @return Empleado
     */
    public function setIngresosAnuales($ingresosAnuales)
    {
        $this->ingresosAnuales = $ingresosAnuales;

        return $this;
    }

    /**
     * Get ingresosAnuales
     *
     * @return string
     */
    public function getIngresosAnuales()
    {
        return $this->ingresosAnuales;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
